﻿using ModeloDDD.Dominio.Model;
using ModeloDDD.Infraestrutura.Data;

class Program
{
    static void Main(string[] args)
    {
         using var ctx = new AppDbContext();


        var celso = new Usuario { Nome = "Celso" };
        var zeus = new Usuario { Nome = "Zeus" };
        var fred = new Usuario { Nome = "fred" };
        var buddie = new Usuario { Nome = "buddie" };

        var ti = new Departamento { NomeDepartamento = "ti", Usuarios = new List<Usuario> { celso, zeus } };
        var rh = new Departamento { NomeDepartamento = "rh", Usuarios = new List<Usuario> { fred } };
        var juridico = new Departamento { NomeDepartamento = "juridico", Usuarios = new List<Usuario> { buddie } };

        //inclui e persiste
        ctx.AddRange(celso, zeus, fred, buddie, ti, rh, juridico);
        ctx.SaveChanges();

        Console.WriteLine();
    }
}
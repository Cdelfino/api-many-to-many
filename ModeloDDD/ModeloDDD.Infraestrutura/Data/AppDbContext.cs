﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using ModeloDDD.Dominio.Model;

namespace ModeloDDD.Infraestrutura.Data
{
    public class AppDbContext : DbContext
    {
        DbSet<Usuario> Usuarios { get; set; }
        DbSet<Departamento> Departamentos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=ModeloDDD;" +
                "Persist Security Info=True;User ID=sa;Password=Cgd221199@")
            .LogTo(Console.WriteLine, new[] { RelationalEventId.CommandExecuted })
            .EnableSensitiveDataLogging();
    }
}

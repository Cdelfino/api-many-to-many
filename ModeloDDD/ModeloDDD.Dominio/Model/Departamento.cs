﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeloDDD.Dominio.Model
{
    public class Departamento
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string NomeDepartamento { get; set; }

        public List<Usuario> Usuarios { get; set; }
    }
}
